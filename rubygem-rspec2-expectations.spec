%global majorver        2.14.5
%global rpmminorver     .%(echo %preminorver | sed -e 's|^\\.\\.*||')
%global fullver %{majorver}%{?preminorver}
%global gem_name        rspec-expectations
%global rpmgem_name     rspec2-expectations
%global gem_minitest    rubygem(minitest4)
%global need_bootstrap_set      0
%{!?need_bootstrap:     %global need_bootstrap  %{need_bootstrap_set}}
Summary:        Rspec 2 expectations (should and matchers)
Name:           rubygem-%{rpmgem_name}
Version:        %{majorver}
Release:        3
License:        MIT
URL:            http://github.com/rspec/rspec-expectations
Source0:        https://rubygems.org/gems/%{gem_name}-%{fullver}.gem
# Backport temporarily be_truthy matchers and so on
Patch0: rubygem-rspec-expectations-2.14.5-be_truthy-alias.patch
# Test suite fix for ruby24 wrt integer unification
Patch1: rubygem-rspec-expectations-2.14.5-ruby24.patch
Patch2: 0001-fix-NameError-compare-failure.patch
Patch3: Fix-uninitialized-constant-and-method-ruby3.2.patch
BuildRequires:  ruby(release)   rubygems-devel
%if 0%{?need_bootstrap} < 1
BuildRequires:  rubygem(rspec2)         %gem_minitest   rubygem(test-unit)
%endif
Provides:       rubygem(%{rpmgem_name}) = %{version}-%{release}
Obsoletes:      rubygem-rspec-expectations < 2.14.5-1
BuildArch:      noarch
%description
rspec-expectations adds `should` and `should_not` to every object and includes
RSpec::Matchers, a library of standard matchers.

%package        doc
Summary:        Documentation for %{name}
Requires:       %{name} = %{version}-%{release}
Obsoletes:      rubygem-rspec-expectations-doc < %{version}-%{release}
%description    doc
This package contains documentation for %{name}.

%prep
gem unpack %{SOURCE0}
%setup -q -D -T -n  %{gem_name}-%{version}
%patch0 -p2
%patch1 -p1
%patch2 -p1
%patch3 -p1
sed -i -e "s@\(require 'test/unit'\)@gem 'minitest', '~> 4' ;\1@" \
        spec/spec_helper.rb
gem specification %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
gem build %{gem_name}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/
rm -f %{buildroot}%{gem_instdir}/{.document,.gitignore,.travis.yml,.yardopts}
%if 0%{?need_bootstrap} < 1

%check
LANG=C.UTF-8
pushd .%{gem_instdir}
ruby -rrubygems -Ilib/ -S rspec2 spec/
popd
%endif

%files
%dir    %{gem_instdir}
%license        %{gem_instdir}/License.txt
%doc    %{gem_instdir}/*.md
%{gem_instdir}/lib/
%exclude        %{gem_cache}
%{gem_spec}

%files  doc
%{gem_docdir}
%{gem_instdir}/features/
%exclude        %{gem_instdir}/spec/

%changelog
* Tue Aug 15 2023 wulei <wu_lei@hoperun.com> - %{majorver}-3
- Fix uninitialized constant and uninitialized method in ruby 3.2

* Tue Jan 17 2023 Ge Wang <wangge20@h-partners.com> - %{majorver}-2
- fix NameError compare failure

* Wed Aug 19 2020 huangyangke <huangyangke@huawei.com> - %{majorver}-1
- package init
